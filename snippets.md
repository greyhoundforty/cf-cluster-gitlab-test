```hcl
resource "local_file" "ansible_hosts" {
  content = <<EOF
[consul_instances]
consul-s1 consul_iface=eth0 consul_node_role=bootstrap ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 0)}
consul-s2 consul_iface=eth0 consul_node_role=server ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 1)}
consul-s3 consul_iface=eth0 consul_node_role=server ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 2)}
consul-c1 consul_iface=eth0 consul_node_role=client ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_client_nodes.*.ipv4_address, 0)}
consul-c2 consul_iface=eth0 consul_node_role=client ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 1)}
consul-c3 consul_iface=eth0 consul_node_role=client ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 2)}

[consul_instances:vars]
host_key_checking = False
ssh_args = -F  /Users/ryan/Sync/Coding/Ansible/ssh.cfg -o ControlMaster=auto -o ControlPersist=30m
control_path = ~/.ssh/ansible-%%r@%%h:%%p

[vault_instances]
vault-c1 vault_iface=eth0 ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_client_nodes.*.ipv4_address, 0)}
vault-c2 vault_iface=eth0 ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 1)}
vault-c3 vault_iface=eth0 ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 2)}

[vault_instances:vars]
host_key_checking = False
ssh_args = -F  /Users/ryan/Sync/Coding/Ansible/ssh.cfg -o ControlMaster=auto -o ControlPersist=30m
control_path = ~/.ssh/ansible-%%r@%%h:%%p

EOF

  filename = "${path.cwd}/inventory.env"
}

data "template_file" "ansible_playbook_template" {
  template = "${file("${path.cwd}/Templates/consul_vault_playbook.yml.tpl")}"

  vars = {
    lbaas_ip        = "${ibm_lbaas.internal_lbaas.vip}"
    acl_token       = "${random_id.acl_token.hex}"
    acl_agent_token = "${random_id.acl_agent_token.hex}"
  }
}

resource "local_file" "ansible_playbook_template_rendered" {
  content = <<EOF
${data.template_file.ansible_playbook_template.rendered}
EOF

  filename = "${path.cwd}/playbooks/consul_vault_playbook.yml"
}

before_script:
  - terraform init -backend-config="lock=true" -backend-config="username=${ETCD_USER}" -backend-config="password=${ETCD_PASSWD}"

```