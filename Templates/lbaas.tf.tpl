resource "ibm_lbaas" "internal_lbaas" {
  name        = "terraformLB"
  description = "delete this"
  subnets     = [2051443]
  type        = "PRIVATE"

  protocols = [{
    frontend_protocol     = "TCP"
    frontend_port         = 80
    backend_protocol      = "TCP"
    backend_port          = 8500
    load_balancing_method = "round_robin"
  }]
}

resource "ibm_lbaas_server_instance_attachment" "lbaas_member" {
  count              = 3
  private_ip_address = "${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address_private,count.index)}"
  weight             = 40
  lbaas_id           = "${ibm_lbaas.internal_lbaas.id}"

  // depends_on         = ["ibm_lbaas.lbaas.id"]
}

output "internal-lb-vip" {
  value = "${ibm_lbaas.internal_lbaas.vip}"
}

