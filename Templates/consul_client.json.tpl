{
  "server": false,
  "datacenter": "dc1",
  "node_name": "${NODE_NAME}",
  "data_dir": "/var/lib/consul",
  "bind_addr": "${BIND_ADDR}",
  "client_addr": "127.0.0.1",
  "retry_join": ["consulserver-1.cloud-design.dev", "consulserver-2.cloud-design.dev", "consulserver-3.cloud-design.dev"],
  "log_level": "DEBUG",
  "enable_syslog": true,
  "acl_enforce_version_8": false
}