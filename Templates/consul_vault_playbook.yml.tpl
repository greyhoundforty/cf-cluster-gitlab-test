- name: Assemble Consul cluster
  hosts: consul_instances
  any_errors_fatal: true
  become: true
  become_user: root
  vars:
    consul_datacenter: "dc1"
    consul_iface: "eth0"
    consul_acl_enable: "true"
    consul_acl_policy: "true"
    consul_acl_datacenter: "dc1"
    consul_acl_agent_master_token: ${agent_token}
  roles:
    - brianshumate.consul
- name: Install Vault
  hosts: vault_nodes
  any_errors_fatal: true
  become: true
  become_user: root
  vars:
    vault_consul: "${lbaas_ip}"
  roles:
    - brianshumate.vault