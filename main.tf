resource "random_id" "lb_name" {
  byte_length = 12
}

resource "random_id" "agent_token" {
  byte_length = 8
}

resource "ibm_compute_vm_instance" "consul_server_nodes" {
  count                      = "3"
  hostname                   = "consul-s${count.index+1}"
  domain                     = "${var.domain}"
  os_reference_code          = "${var.os_reference_code["u16"]}"
  datacenter                 = "${var.datacenter["us-east3"]}"
  network_speed              = 1000
  hourly_billing             = true
  private_network_only       = true
  user_metadata              = "${file("server_install.sh")}"
  flavor_key_name            = "${var.flavor_key_name["medium"]}"
  tags                       = ["ryantiffany", "consul-server"]
  local_disk                 = false
  ssh_key_ids                = ["${data.ibm_compute_ssh_key.tycho_Key.id}", "${data.ibm_compute_ssh_key.athena_Key.id}"]
}

resource "ibm_compute_vm_instance" "consul_client_nodes" {
  count                      = "3"
  hostname                   = "consul-c${count.index+1}"
  domain                     = "${var.domain}"
  os_reference_code          = "${var.os_reference_code["u16"]}"
  datacenter                 = "${var.datacenter["us-east3"]}"
  network_speed              = 1000
  hourly_billing             = true
  private_network_only       = true
  user_metadata              = "${file("client_install.sh")}"
  flavor_key_name            = "${var.flavor_key_name["medium"]}"
  tags                       = ["ryantiffany", "consul-client", "vault-server"]
  local_disk                 = false
  ssh_key_ids                = ["${data.ibm_compute_ssh_key.tycho_Key.id}", "${data.ibm_compute_ssh_key.athena_Key.id}"]
}

resource "dnsimple_record" "consul_client_dns" {
  count  = "3"
  domain = "${var.domain}"
  name   = "consul-c${count.index+1}"
  value  = "${element(ibm_compute_vm_instance.consul_client_nodes.*.ipv4_address_private,count.index)}"
  type   = "A"
  ttl    = 3600
}

resource "dnsimple_record" "consul_server_dns" {
  count  = "3"
  domain = "${var.domain}"
  name   = "consul-s${count.index+1}"
  value  = "${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address_private,count.index)}"
  type   = "A"
  ttl    = 3600
}

// resource "dnsimple_record" "consul_lb_dns" {
//   domain = "${var.domain}"
//   name   = "private-consul-lb"
//   value  = "${ibm_lbaas.internal_lbaas.vip}"
//   type   = "CNAME"
//   ttl    = 3600
// }

// resource "ibm_lbaas" "internal_lbaas" {
//   name        = "${random_id.lb_name.hex}"
//   description = "Private consul load balancer demo - RT"
//   subnets     = ["${element(ibm_compute_vm_instance.consul_server_nodes.*.private_subnet_id, 0)}"]
//   type        = "PRIVATE"

//   protocols = [{
//     frontend_protocol     = "TCP"
//     frontend_port         = 80
//     backend_protocol      = "TCP"
//     backend_port          = 8500
//     load_balancing_method = "round_robin"
//   }]
// }

// resource "ibm_lbaas_server_instance_attachment" "lbaas_member" {
//   count              = 3
//   private_ip_address = "${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address_private,count.index)}"
//   weight             = 40
//   lbaas_id           = "${ibm_lbaas.internal_lbaas.id}"
// }

// resource "ibm_security_group" "allow_consul_traffic_in" {
//   name        = "consul_allow"
//   description = "allow consul cluster traffic - RT"
// }

// resource "ibm_security_group_rule" "allow_port_8500_in" {
//   direction         = "ingress"
//   ether_type        = "IPv4"
//   port_range_min    = 8500
//   port_range_max    = 8500
//   protocol          = "tcp"
//   security_group_id = "${ibm_security_group.allow_consul_traffic_in.id}"
// }

// resource "ibm_security_group_rule" "allow_port_8600_in" {
//   direction         = "ingress"
//   ether_type        = "IPv4"
//   port_range_min    = 8600
//   port_range_max    = 8600
//   protocol          = "tcp"
//   security_group_id = "${ibm_security_group.allow_consul_traffic_in.id}"
// }

// resource "ibm_security_group_rule" "allow_serf_ports_in" {
//   direction         = "ingress"
//   ether_type        = "IPv4"
//   port_range_min    = 8300
//   port_range_max    = 8302
//   protocol          = "tcp"
//   security_group_id = "${ibm_security_group.allow_consul_traffic_in.id}"
// }

