data "ibm_compute_ssh_key" "tycho_Key" {
  label = "ryan_tycho"
}

data "ibm_compute_ssh_key" "athena_Key" {
  label = "ryan_athena"
}

// data "ibm_security_group" "allow_outbound" {
//   name = "allow_outbound"
// }

// data "ibm_security_group" "allow_ssh" {
//   name = "allow_ssh"
// }
