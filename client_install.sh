#!/bin/bash -v

DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get upgrade -y 
DEBIAN_FRONTEND=noninteractive apt-get install unzip -y 

CONSUL_VERSION="1.5.0"
curl --silent --remote-name https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
unzip consul_${CONSUL_VERSION}_linux_amd64.zip
chown root:root consul
mv consul /usr/local/bin/
consul -autocomplete-install


useradd --system --home /etc/consul.d --shell /bin/false consul
mkdir --parents /var/lib/consul
chown --recursive consul:consul /var/lib/consul
mkdir --parents /etc/consul.d

cat <<EOF >/etc/consul.d/client.json

{
    "datacenter": "dc1",
    "data_dir": "/var/lib/consul",
    "retry_join": ["consul-s1.cdetesting.com", "consul-s2.cdetesting.com", "consul-s3.cdetesting.com"],
    "acl": {
          "enabled": true,
          "default_policy": "allow",
          "enable_token_persistence": true
        }
}

EOF

chown --recursive consul:consul /etc/consul.d

cat <<EOF >/etc/systemd/system/consul.service
[Unit]
Description="HashiCorp Consul - A service mesh solution"
Documentation=https://www.consul.io/
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/consul.d/client.json

[Service]
User=consul
Group=consul
ExecStart=/usr/local/bin/consul agent -config-dir=/etc/consul.d/
ExecReload=/usr/local/bin/consul reload
KillMode=process
Restart=on-failure
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF
}

systemctl enable consul.service
systemctl start consul.service

