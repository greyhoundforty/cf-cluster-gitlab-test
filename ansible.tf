// resource "local_file" "ansible_hosts" {
//   content = <<EOF
// [consul_instances]
// consul-s1 consul_iface=eth0 consul_node_role=bootstrap ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 0)}
// consul-s2 consul_iface=eth0 consul_node_role=server ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 1)}
// consul-s3 consul_iface=eth0 consul_node_role=server ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 2)}
// consul-c1 consul_iface=eth0 consul_node_role=client ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_client_nodes.*.ipv4_address, 0)}
// consul-c2 consul_iface=eth0 consul_node_role=client ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 1)}
// consul-c3 consul_iface=eth0 consul_node_role=client ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 2)}

// [consul_instances:vars]
// host_key_checking = False
// ssh_args = -F  /Users/ryan/Sync/Coding/Ansible/ssh.cfg -o ControlMaster=auto -o ControlPersist=30m
// control_path = ~/.ssh/ansible-%%r@%%h:%%p

// [vault_instances]
// vault-c1 vault_iface=eth0 ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_client_nodes.*.ipv4_address, 0)}
// vault-c2 vault_iface=eth0 ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 1)}
// vault-c3 vault_iface=eth0 ansible_ssh_user=ryan ansible_host=${element(ibm_compute_vm_instance.consul_server_nodes.*.ipv4_address, 2)}

// [vault_instances:vars]
// host_key_checking = False
// ssh_args = -F  /Users/ryan/Sync/Coding/Ansible/ssh.cfg -o ControlMaster=auto -o ControlPersist=30m
// control_path = ~/.ssh/ansible-%%r@%%h:%%p

// EOF

//   filename = "${path.cwd}/inventory.env"
// }